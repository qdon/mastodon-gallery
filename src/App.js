import React, { Component } from 'react';
import './App.css';
import Masonry from 'react-masonry-component';
import classNames from 'classnames';
import { Helmet } from 'react-helmet';

const flattenMedia = data => {
  const localMax = Math.max.apply(null, data.map(a => a.favourites_count + a.reblogs_count));
  return data.reduce((arr, cur) => arr.concat(cur.media_attachments.filter(a => a.type === 'image').map(a => Object.assign({}, a, { status: cur, highlighted: cur.media_attachments.length === 1 && (cur.favourites_count + cur.reblogs_count >= localMax) }))), []);
};

const lastId = data => data.length > 0 ? data[data.length - 1].id : false;
const firstId = data => data.length > 0 ? data[0].id : false;

const defaultConfig = {
  title: 'Gallery',
  baseURL: '/',
  hashtag: null,
  onlyLocal: true,
  dontCareNsfw: false,
  autoRefresh: false,
};


class SubmitableInput extends Component {
  constructor(props) {
    super();
    const value = props.value || '';
    this.state = {
      inputValue: value,
      lastValue: value,
    };
    this.onKeyUp = this.onKeyUp.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  onKeyUp(event) {
    if (event.key === 'Enter') {
      event.target.blur();
    }
  }

  onBlur(_e) {
    const { lastValue, inputValue } = this.state;
    if (lastValue !== inputValue) {
      this.props.onSubmit(this.state.inputValue);
      this.setState({
        lastValue: inputValue,
      });
    }
  }

  onChange(e) {
    this.setState({
      inputValue: e.target.value,
    });
  }

  render() {
    const { inputValue } = this.state;

    return (
      <input
        type="text"
        value={ inputValue }
        onBlur={ this.onBlur }
        onKeyUp={ this.onKeyUp }
        onChange={ this.onChange.bind(this) }
      />
    );
  }
}

class App extends Component {

  constructor (props) {
    super(props);

    this.configFromUrl = this.configFromUrl.bind(this);
    this.handleScroll    = this.handleScroll.bind(this);
    this.loadMore        = this.loadMore.bind(this);
    this.loadNew         = this.loadNew.bind(this);
    this.loadInitialPage = this.loadInitialPage.bind(this);
    this.pagePath        = this.pagePath.bind(this);
    this.nextPageUrl  = this.nextPageUrl.bind(this);
  }

  state = {
    config: defaultConfig,
    initialized: false,
    loading: true,
    items: [],
    nextId: false,
    prevId: false,
    autoRefreshHandle: null,
  };

  componentDidMount () {
    fetch(
      'config.json').then(
        res => res.json()
      ).then(data => {
        this.setState({config: data});
      }).finally(() => {
        this.configFromUrl();
        this.loadInitialPage();
      });

    window.addEventListener('scroll', this.handleScroll);
  }

  configFromUrl() {
    let search = window.location.search;
    let params = new URLSearchParams(search);
    let { config } = this.state;
    for (const [key, val] of params.entries()) {
      config[key] = val;
    }

    if (config.onlyLocal !== undefined) {
      config.onlyLocal = config.onlyLocal !== 'false';
    }

    this.setState({ config });
  }

  handleChangeBaseURL(value) {
    let { config } = this.state;
    config.baseURL = value;
    this.reloadContents(config);
  }

  handleChangeHashtag(value) {
    let { config } = this.state;
    config.hashtag = value;
    this.reloadContents(config);
  }

  handleChangeOnlyLocal(e) {
    let { config } = this.state;
    config.onlyLocal = e.target.checked;
    this.reloadContents(config);
  }

  handleChangeNsfw = (e) => {
    let { config } = this.state;
    config.dontCareNsfw = e.target.checked;
    this.setState({config});
  }

  handleChangeAutoRefresh(e) {
    const checked = e.target.checked;
    let { config, autoRefreshHandle } = this.state;
    config.autoRefresh = checked;
    if (checked) {
      if (autoRefreshHandle) {
        clearInterval(autoRefreshHandle);
      }
      autoRefreshHandle = setInterval(this.loadNew.bind(this), 10 * 1000);
    } else {
      if (autoRefreshHandle) {
        clearInterval(autoRefreshHandle);
      }
      autoRefreshHandle = null;
    }

    this.setState({config, autoRefreshHandle});
  }

  reloadContents(config) {
    this.setState({config, nextId: undefined}, () => {
      this.loadInitialPage();
    });
    let params = new URLSearchParams();
    if (config.baseURL !== '/') {
      params.append('baseURL', config.baseURL);
    }
    params.append('onlyLocal', config.onlyLocal);
    if (config.hashtag) {
      params.append('hashtag', config.hashtag);
    }
    window.history.replaceState(null, null, `?${params.toString()}`);
  }

  handleScroll () {
    const { scrollHeight, scrollTop, clientHeight } = document.documentElement;
    const offset = scrollHeight - scrollTop - clientHeight;

    if(offset < 400 && this.state.nextId && !this.state.loading) {
      this.loadMore();
    }
  }

  loadInitialPage () {
    fetch(
      this.nextPageUrl(), {
        credentials: 'same-origin',
        referrerPolicy: 'same-origin',
      }).then(
        res => res.json()
      ).then(data => {
        this.setState({
          items: flattenMedia(data),
          loading: false,
          nextId: lastId(data),
          prevId: firstId(data),
          initialized: true,
        })
      });
  }

  pagePath () {
    const { baseURL, hashtag } = this.state.config;

    if (hashtag) {
      return `${baseURL}/api/v1/timelines/tag/${hashtag}`;
    } else {
      return `${baseURL}/api/v1/timelines/public`;
    }
  }

  nextPageUrl () {
    const { config: { onlyLocal }, nextId } = this.state;

    let url = new URL(this.pagePath());
    let params = {
      local: onlyLocal,
      only_media: true,
      max_id: nextId || undefined,
    };

    Object.entries(
      params
    ).filter(
      ([key, val]) => val != null
    ).forEach(([key, val]) => {
      url.searchParams.append(key, val);
    });

    return url;
  }

  newPageUrl () {
    const { config: { onlyLocal }, prevId } = this.state;

    let url = new URL(this.pagePath());
    let params = {
      local: onlyLocal,
      only_media: true,
      since_id: prevId || undefined,
    };

    Object.entries(
      params
    ).filter(
      ([key, val]) => val != null
    ).forEach(([key, val]) => {
      url.searchParams.append(key, val);
    });

    return url;
  }

  loadMore () {
    if (!this.state.nextId) {
      return;
    }

    this.setState({ loading: true });

    fetch(
      this.nextPageUrl(), {
        credentials: 'same-origin',
        referrerPolicy: 'same-origin',
      }).then(
        res => res.json()
      ).then(data => {
        this.setState({
          items: this.state.items.concat(flattenMedia(data)),
          loading: false,
          nextId: lastId(data),
        });
      });
  }

  loadNew() {
    this.setState({ loading: true });

    fetch(
      this.newPageUrl(), {
        credentials: 'same-origin',
        referrerPolicy: 'same-origin',
      }).then(
        res => res.json()
      ).then(data => {
        let items = flattenMedia(data).concat(this.state.items);

        this.setState({
          items,
          loading: false,
          prevId: firstId(data) || this.state.prevId,
        });
      });
  }

  renderItem (item) {
    const previewUrl    = item.preview_url;
    const previewWidth  = item.meta ? item.meta.small.width : 400;
    const originalUrl   = item.url;
    const originalWidth = item.meta ? item.meta.original.width : 800;
    const originalHeight = item.meta ? item.meta.original.height : 600;
    const displayName   = item.status.account.display_name.trim().length > 0 ? item.status.account.display_name : item.status.account.acct.split('@')[0];
    const isNsfw = item.status.sensitive;

    return (
      <div key={originalUrl} className={classNames('item', { 'item--larger': item.highlighted }, {'nsfw': isNsfw})}>
        <div className="item__image_container">
          <img className={classNames('item__image')} src={previewUrl} srcSet={`${originalUrl} ${originalWidth}w, ${previewUrl} ${previewWidth}w`} sizes={item.highlighted ? 'calc(0.4 * 100vw)' : 'calc(0.2 * 100vw)'} alt={item.description} width={originalWidth} height={originalHeight} />
        </div>

        <a className='item__source' href={item.status.url} target='_blank' rel='noopener noreferrer'>
          <img src={item.status.account.avatar} alt={item.status.account.acct} width={36} height={36} />

          <div>
            <bdi>{displayName}</bdi>
            @{item.status.account.acct}
          </div>
        </a>
      </div>
    );
  }

  render() {
    const { initialized, items, config } = this.state;

    if (!initialized) {
      return (
        <div className='loading'>
          <p>Loading&hellip;</p>
        </div>
      );
    }

    const options = {
      itemSelector: '.item',
      columnWidth: '.sizer',
      percentPosition: true,
    };

    return (
      <div className='grid'>
        <Helmet>
          <title>{config.title}</title>
        </Helmet>

        <label>
          baseURL
          <SubmitableInput
            value={this.state.config.baseURL}
            onSubmit={this.handleChangeBaseURL.bind(this)} />
        </label>
        <label>
          hashtag
          <SubmitableInput
            value={this.state.config.hashtag}
            onSubmit={this.handleChangeHashtag.bind(this)} />
        </label>
        <label>
          onlyLocal
          <input
            type="checkbox"
            checked={this.state.config.onlyLocal === true}
            onChange={this.handleChangeOnlyLocal.bind(this)} />
        </label>
        <label>
          I don't care about NSFW
          <input
            type="checkbox"
            checked={this.state.config.dontCareNsfw === true}
            onChange={this.handleChangeNsfw.bind(this)} />
        </label>
        <label>
          Auto refresh
          <input
            type="checkbox"
            checked={this.state.config.autoRefresh === true}
            onChange={this.handleChangeAutoRefresh.bind(this)} />
        </label>


        <Masonry options={options} className={this.state.config.dontCareNsfw && 'show-nsfw'}>
          <div className='sizer' />

          {items.map(item => this.renderItem(item))}
        </Masonry>
      </div>
    );
  }
}

export default App;
